-- 默认按键
-- grave 1234567890 minus equal BackSpace
-- Tab qwertyuiop bracketleft bracketright backslash
-- Escape asdfghjkl semicolon apostrophe
-- Shift_L zxcvbnm comma period slash

-- Shift 层按键
-- asciitilde exclam at numbersign dollar percent asciicircum ampersand asterisk parenleft parenright underscore plus BackSpace
-- ISO_Left_Tab Q W E R T Y U I O P braceleft braceright bar
-- Escape A S D F G H J K L colon quotedbl
-- Shift_L Z X C V B N M less greater question

local keymap = {}
local qwerty = "qwertyuioasdfghjklzxcvbnm"
local colemak = "qwfpgjluyarstdhneizxcvbkm"
for i = 1, #qwerty do
	keymap[qwerty:sub(i, i)] = colemak:sub(i, i)
	keymap["Shift+" .. string.upper(qwerty:sub(i, i))] = string.upper(colemak:sub(i, i))
end
keymap["p"] = ":"
keymap["Shift+P"] = ";"
keymap["semicolon"] = "o"
keymap["Shift+colon"] = "O"
keymap["apostrophe"] = '"'
keymap["Shift+quotedbl"] = "'"

local function processor(key, env)
	local engine = env.engine
	local context = engine.context

	local keyrepr = key:repr()

	if context:get_option("ascii_mode") then
		if key:release() or key:alt() or key:super() or key:ctrl() then
			return 2
		end
		local value = keymap[keyrepr]
		if value ~= nil then
			engine:commit_text(value)
			return 1
		end
	end
	return 2
end

return { func = processor }
