local function charset(char)
	if char >= 0x3400 and char <= 0x4dbf then
		return "CJK-A"
	elseif char >= 0x20000 and char <= 0x2a6df then
		return "CJK-B"
	elseif char >= 0x2a700 and char <= 0x2b73f then
		return "CJK-C"
	elseif char >= 0x2b740 and char <= 0x2b81f then
		return "CJK-D"
	elseif char >= 0x2b820 and char <= 0x2ceaf then
		return "CJK-E"
	elseif char >= 0x2ceb0 and char <= 0x2ebef then
		return "CJK-F"
	elseif char >= 0x30000 and char <= 0x3134f then
		return "CJK-G"
	elseif char >= 0x31350 and char <= 0x323af then
		return "CJK-H"
	elseif char >= 0x2ebf0 and char <= 0x2ee5d then
		return "CJK-I"
	elseif char >= 0xf900 and char <= 0xfaff then
		return "兼容"
	elseif char >= 0x2f800 and char <= 0x2fa1f then
		return "兼容补充"
	elseif char >= 0x2f00 and char <= 0x2fdf then
		return "康𤋮部首"
	elseif char >= 0x2e80 and char <= 0x2eff then
		return "部首补充"
	end
	return ""
end

local function filter(input, env)
	for cand in input:iter() do
		if cand.type == "table" then
			cand:get_genuine().comment = cand.comment .. charset(utf8.codepoint(cand.text))
		end
		yield(cand)
	end
end

return filter
