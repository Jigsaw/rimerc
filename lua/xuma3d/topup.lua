local function topup(env)
	local candidate = env.engine.context:get_selected_candidate()
	if candidate and not (candidate.type == "smart" and utf8.len(candidate.text) == 1) then
		env.engine:commit_text(candidate.text:sub(1, utf8.offset(candidate.text, 2) - 1))
	end
	env.engine.context.input = env.engine.context.input:sub(env.engine.context:get_option("四码优先") and 5 or 4)
end

local function processor(key_event, env)
	local engine = env.engine
	local context = engine.context

	local input = context.input
	local input_len = #input

	if key_event:release() or key_event:ctrl() or key_event:alt() then
		return 2
	end

	local ch = key_event.keycode
	if ch < 0x61 or ch >= 0x7f then
		return 2
	end

	local max_code_length = env.engine.context:get_option("只出三码") and 3 or 4
	if input_len >= max_code_length then
		topup(env)
	end

	return 2
end

return processor
