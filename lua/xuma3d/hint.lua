local function filter(input, env)
	for cand in input:iter() do
		if cand.type == "smart" and #env.engine.context.input < 6 then
			cand:get_genuine().comment = "🧩"
		end

		if cand.type == "table" then
			if #env.engine.context.input < 3 then
				cand:get_genuine().comment = "🧩"
			else
				local lookup = " " .. env.reverse:lookup(cand.text) .. " "
				local short = string.match(lookup, " ([a-z]) ") or string.match(lookup, " ([a-z][a-z]) ")
				if short then
					cand:get_genuine().comment = "〔" .. short .. "〕"
				end
			end
		end

		yield(cand)
	end
end

local function init(env)
	local dict = env.engine.schema.config:get_string("translator/dictionary")
	env.reverse = ReverseLookup(dict)
end

return { init = init, func = filter }
