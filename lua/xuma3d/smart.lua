local function translator(input, seg, env)
	if
		seg:has_tag("reverse_lookup")
		or seg:has_tag("unicode")
		or #input <= 3
		or env.engine.context:get_option("四码优先")
	then
		return
	end
	local char
	if not env.memory:dict_lookup(input:sub(1, 3), false, 1) then
		char = ""
	end
	for entry in env.memory:iter_dict() do
		char = entry.text
		break
	end
	if env.memory:dict_lookup(input:sub(4), false, 1) then
		for entry in env.memory:iter_dict() do
			local cand = Candidate("smart", seg.start, seg._end, char .. entry.text, "")
			cand.quality = 1
			yield(cand)
		end
	end
end

local function init(env)
	env.memory = Memory(env.engine, env.engine.schema)
end

return { init = init, func = translator }
