# 自用 Rime 配置

- 徐码：[forFudan/xuma](https://github.com/forFudan/xuma)
- 星空键道6：[xkinput/Rime_JD](https://github.com/xkinput/Rime_JD)
- 闽南语：[a-thok/rime-hokkien](https://github.com/a-thok/rime-hokkien)
- 同文输入法皮肤：[SivanLaai/rime-pure](https://github.com/SivanLaai/rime-pure)
