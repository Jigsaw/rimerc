# Rime schema settings
# encoding: utf-8

schema:
  schema_id: xuma3d
  name: 尔雅·徐码·三码顶
  version: 20230515
  author:
    - 发明人 徐国银先生
    - 拆分表 forFudan
    - 三码方案 Jigsaw
  description: |
    尔雅·徐码输入法
    官方网址：<http://xumax.cn>
    官方QQ群 761401688
    forFudan 修订方案 <https://github.com/forFudan/xuma>
    特点:
    - 提供CJK至I區汉字全字拆分.
    码表根据
      (1) 徐码作者徐国银提供的《徐码CJK字词码表》2023年1月15日版
        官方网址：http://xumax.cn/
      (2) 宇浩輸入法 CJK-H CJK-I 拆分表
        https://zhuyuhao.com/yuhao/
      修订而成
  chaifen: |
    本拆分表基於徐碼官方碼表（2023年1月15日版本）製作。
    本拆分表包含至 CJK-I 區的所有漢字的拆分。
    本拆分表爲全字部件拆分表，卽包含所有構字部件。
    拆分後附有徐碼，以大小寫區分大小碼。
    本拆分表經過字形校驗。字形標準取自：
      -《中華人民共和國國家標準 GB 18030--2022 信息技術 中文編碼字符集》中刊載的漢字。
      - The Unicode Standard, Version 15.0 中刊載的漢字字形。
    末尾標注 ⁇ 號的拆分方法，同官方碼表存在不同，需要進一步核查。
    未尾標注 ✓ 號的拆分方法，同官方碼表存在不同，但根據標準字形進行了修正。
    未尾標注 ⊖ 號的拆分方法，官方碼表没有收録，本拆分表給予補充。

switches:
  - options:
      - 只出三码
      - 保留四码
      - 四码优先
    states: [只出三码, 保留四码, 四码优先]
    reset: 1
  - name: traditionalization
    reset: 0
    states: [反查简, 反查繁]
  - name: chaifen
    reset: 1
  - name: ascii_punct
    reset: 0
  - name: ascii_mode
    states: [中文, 西文]
  - name: full_shape
    reset: 0

engine:
  processors:
    - ascii_composer
    - recognizer
    - key_binder
    - lua_processor@*jiandao/smart_selector
    - lua_processor@*xuma3d/topup
    - speller
    - punctuator
    - selector
    - navigator
    - express_editor
  segmentors:
    - ascii_segmentor
    - matcher
    - abc_segmentor
    - punct_segmentor
    - fallback_segmentor
  translators:
    - punct_translator
    - table_translator
    - lua_translator@*xuma3d/smart
    - lua_translator@*jiandao/unicode_translator
    - reverse_lookup_translator
  filters:
    - simplifier@traditionalize
    - simplifier@chaifen
    - lua_filter@*xuma3d/hint
    - uniquifier

traditionalize:
  tags:
    - reverse_lookup
  option_name: traditionalization
  opencc_config: s2t.json
  tips: none

speller:
  alphabet: zyxwvutsrqponmlkjihgfedcba
  initials: zyxwvutsrqponmlkjihgfedcba
  delimiter: " '"
  auto_select: false

translator:
  dictionary: xuma3d
  prism: xuma3d
  enable_completion: false
  enable_sentence: false
  enable_user_dict: false
  enable_encoder: false

reverse_lookup:
  dictionary: jiandao.base
  prefix: "`"
  preedit_format:
    - xform/([nljqxy])v/$1ü/

chaifen:
  opencc_config: xuma3d_chaifen.json
  option_name: chaifen
  show_in_comment: true
  tags:
    - reverse_lookup
    - unicode
  tips: char

punctuator:
  import_preset: symbols

key_binder:
  bindings:
    - { when: has_menu, accept: minus, send: Page_Up }
    - { when: has_menu, accept: equal, send: Page_Down }

recognizer:
  patterns:
    reverse_lookup: "^`([a-z]+?)*$"
    unicode: "^u`[0-9a-f]*$"
